#ifndef SKISEMINAR_GLDISPLAY_HH
#define SKISEMINAR_GLDISPLAY_HH

#include <complex>
#include <chrono>
#include <cstddef>
#include <functional>
#include <future>
#include <string>
#include <utility>
#include <vector>

#include <GL/gl.h>

#include "pnm.hh"

class GLDisplay {
  using Clock = std::chrono::system_clock;

  struct DrawArea {
    std::complex<double> origin;
    double pxsize;
    unsigned width, height;
  };

  struct NewData {
    DrawArea area;
    std::vector<GLfloat> data;
    Clock::duration duration;
    std::size_t generator_id;
    std::size_t maxiter;
  };

  //
  // state
  //

  // current area of window
  DrawArea winarea;

  std::size_t current_generator;

  // area of current texture
  DrawArea texarea;
  std::size_t maxiter;

  // whether a redraw is needed
  // this is not set for redraws that are already under way, only for ones
  // that did not start yet
  bool needsRedraw;

  // passing back data from computing thread
  std::future<NewData> newData;

  struct {
    GLuint vertex, element;
  } buffer;

  GLuint texture;

  GLuint program;

  struct {
    GLint texture;
    GLint texorigin, texsize;
    GLint winorigin, winsize;
  } uniforms;

  struct {
    GLint position;
  } attributes;

  struct {
    bool dragging;
    int x, y;
  } mouse;

  struct {
    bool isFull;
    int oldwidth, oldheight;
  } fullscreen;

  static GLDisplay *instance_;

  //
  // config
  //
  using Generator = PPM(std::complex<double>, double,
                        std::size_t, std::size_t, std::size_t);
  struct GeneratorInfo {
    std::string name;
    std::function<Generator> generator;
  };
  std::vector<GeneratorInfo> generators;

  double zoomFactor = 0.9;

  NewData genBuffer(DrawArea drawarea, std::size_t generator_id,
                    std::size_t maxiter) const;
  // typically called with the result from genBuffer
  void setTexture(const NewData &data);

  // kick of a redraw, or queue if already in progress
  void postRedraw();

  // zoom by factor around mouse position (x,y)
  void zoom(double factor, int x, int y);

  void toggleFullScreen();
  void cycleGenerator();
  void setMaxIter(std::size_t maxiter_);

  // initialize all runtime state
  void initState();

  //callbacks
  void idle();
  void keyboard(unsigned char key, int x, int y);
  void keyboard_s(int key, int x, int y);
  void mouse_button(int button, int action, int x, int y);
  void mouse_motion(int x, int y);
  void render() const;
  void reshape(int width, int height);

  struct CallBack;

public:
  void run(int argc, char **argv);

  void addGenerator(std::string name, std::function<Generator> gen)
  {
    generators.push_back({ std::move(name), std::move(gen) });
  }

};

#endif // SKISEMINAR_GLDISPLAY_HH
