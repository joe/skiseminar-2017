#include <complex>
#include <cstddef>
#include <string>

#include "gldisplay.hh"
#include "mandelbrot.hh"
#include "simd/simd.hh"
#include "simd/vc.hh"

using namespace Dune::Simd;

template<class V>
PPM generator(std::complex<Scalar<V> > origin, Scalar<V> pxsize,
              std::size_t width, std::size_t height, std::size_t maxiter)
{
  // just add the colormap
  return mandelbrot<V>(origin, pxsize, width, height,
                       maxiter, SmoothColorMap{96});
}

int main(int argc, char **argv)
{
  GLDisplay display;

  // add Vc generator
  using V = Vc::Vector<double>;
  std::string name = "Vc<"+std::to_string(lanes<V>())+">";
  display.addGenerator(name, generator<V>);

  // add Standard generator
  display.addGenerator("Standard", generator<double>);

  display.run(argc, argv);
}
