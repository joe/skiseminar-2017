#ifndef SKISEMINAR_MANDELBROT_HH
#define SKISEMINAR_MANDELBROT_HH

#include <complex>
#include <cstddef>
#include <cstdint>

#include "color.hh"
#include "pnm.hh"
#include "simd/simd.hh"
#include "util.hh"

using namespace Dune::Simd;

// do the mandelbrot iteration for point c
// stop when the escape criterion is fullfilled or maxiter is reached
// return number of iterations done
template<class V>
auto iterate(std::complex<V> c, std::size_t maxiter)
{
  // always start from 0+0i
  std::complex<V> z(V(0));

  // record the number of iterations until escape for each lane
  Index<V> iter(maxiter);
  // record which lanes did escape
  Mask<V> escaped(false);

  for(std::size_t i = 0; i < maxiter; ++i)
  {
    // do the iteration for all lanes (even escaped ones)
    z = z*z + c;

    // check whether any lanes escaped
    auto just_escaped = abs2(z) > V(4) && !escaped;
    // record iteration count of escape
    iter = cond(just_escaped, Index<V>(i), iter);

    // remember newly escaped lanes
    escaped |= just_escaped;
    // stop if all lanes have escaped
    if(allTrue(escaped))
      break;
  }

  return iter;
}

// compute an image of a mandelbrot set for the given area
// use colorMap to assign colors to the number of iterations taken
template<class V, class ColorMap>
auto mandelbrot(std::complex<Scalar<V> > origin, Scalar<V> pxsize,
                std::size_t width, std::size_t height,
                std::uint16_t maxiter, const ColorMap &colorMap)
{
  // image to store result in
  PPM result(width, height, colorMap.maxcolor());

  // row position
  auto y = origin.imag();
  for(std::size_t row = 0; row < height; ++row)
  {
    // reset column position(s)
    auto x = origin.real() + sequence<V>() * pxsize;
    for(std::size_t col = 0; col < width; col += lanes(x))
    {
      // run the computation
      auto iter = iterate(std::complex<V>{ x, y }, maxiter);

      // break vectorization
      for(std::size_t l = 0; l < lanes(x) && col + l < width; ++l)
        // update image
        result(row, col+l) = colorMap(lane(l, iter), maxiter);

      // advance column position(s)
      x += pxsize * lanes(x);
    }

    // advance row position
    y += pxsize;
  }

  return result;
}

#endif // SKISEMINAR_MANDELBROT_HH
