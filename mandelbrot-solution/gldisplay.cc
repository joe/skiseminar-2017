#include <algorithm>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <future>
#include <iostream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>

#include "fragment-shader.hh"
#include "gldisplay.hh"
#include "vertex-shader.hh"

//
// Utilities
//

static void show_info_log(GLuint object,
                          PFNGLGETSHADERIVPROC glGet__iv,
                          PFNGLGETSHADERINFOLOGPROC glGet__InfoLog)
{
  GLint log_length;
  glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);

  std::vector<char> log(log_length);
  glGet__InfoLog(object, log_length, nullptr, log.data());

  std::cerr.write(log.data(), log.size());
}

static GLuint make_buffer(GLenum target,
                          const void *buffer_data,
                          GLsizei buffer_size)
{
  GLuint buffer;
  glGenBuffers(1, &buffer);
  glBindBuffer(target, buffer);
  glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
  return buffer;
}

static GLuint make_shader(GLenum type, const std::string &src)
{
  GLuint shader = glCreateShader(type);
  GLint size = src.size();
  const GLchar *source = src.data();
  glShaderSource(shader, 1, (const GLchar**)&source, &size);
  glCompileShader(shader);

  GLint shader_ok;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
  if (!shader_ok) {
    std::cerr << "Failed to compile:" << std::endl;
    show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
    std::abort();
  }
  return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader)
{
  GLuint program = glCreateProgram();
  glAttachShader(program, vertex_shader);
  glAttachShader(program, fragment_shader);
  glLinkProgram(program);

  GLint program_ok;
  glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
  if (!program_ok) {
    std::cerr << "Failed to link shader program:" << std::endl;
    show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
    std::abort();
  }
  return program;
}

//
// members of GLDisplay
//

GLDisplay *GLDisplay::instance_ = nullptr;

struct GLDisplay::CallBack
{
  static void idle()
  {
    instance_->idle();
  }
  static void keyboard(unsigned char key, int x, int y)
  {
    instance_->keyboard(key, x, y);
  }
  static void keyboard_s(int key, int x, int y)
  {
    instance_->keyboard_s(key, x, y);
  }
  static void mouse_button(int button, int action, int x, int y)
  {
    instance_->mouse_button(button, action, x, y);
  }
  static void mouse_motion(int x, int y)
  {
    instance_->mouse_motion(x, y);
  }
  static void render()
  {
    instance_->render();
  }
  static void reshape(int width, int height)
  {
    instance_->reshape(width, height);
  }
};

GLDisplay::NewData
GLDisplay::genBuffer(DrawArea drawarea, std::size_t generator_id,
                     std::size_t maxiter) const
{
  const auto &generator = generators[generator_id].generator;
  Clock clock;
  auto start = clock.now();
  auto img = generator(drawarea.origin, drawarea.pxsize,
                       drawarea.width, drawarea.height, maxiter);
  auto stop = clock.now();
  NewData result;
  result.area = drawarea;

  result.data.reserve(3*drawarea.width*drawarea.height);
  GLfloat maxval = img.maxval();
  for(std::size_t r = 0; r < drawarea.height; ++r)
    for(std::size_t c = 0; c < drawarea.width; ++c)
    {
      const auto &px = img(r,c);
      result.data.push_back(px.red  /maxval);
      result.data.push_back(px.green/maxval);
      result.data.push_back(px.blue /maxval);
    }

  result.duration = stop-start;
  result.generator_id = generator_id;
  result.maxiter = maxiter;

  return result;
}

void GLDisplay::setTexture(const NewData &data)
{
  texarea = data.area;

  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0,                 /* target, level of detail */
               GL_RGB8,                          /* internal format */
               texarea.width, texarea.height, 0, /* width, height, border */
               GL_RGB, GL_FLOAT,                 /* external format, type */
               data.data.data());                /* pixels */

  decltype(texarea.origin) upper = {
    texarea.origin.real() + texarea.pxsize * texarea.width,
    texarea.origin.imag() + texarea.pxsize * texarea.height,
  };
  using std::chrono::duration_cast;
  using std::chrono::milliseconds;
  std::cout << "Generator: " << generators[data.generator_id].name
            << ", redraw took "
            << duration_cast<milliseconds>(data.duration).count()
            << "ms, maxiter=" << data.maxiter << ", area=" << texarea.origin
            << ".." << upper << ", pixels=(" << texarea.width << ","
            << texarea.height << ")" << std::endl;
}

void GLDisplay::postRedraw()
{
  if(newData.valid())
  {
    // already in progress
    needsRedraw = true;
  }
  else
  {
    // kick off redraw
    newData = std::async(std::launch::async,
                         &GLDisplay::genBuffer, this, winarea,
                                                current_generator, maxiter);
    needsRedraw = false;
    glutIdleFunc(CallBack::idle);
  }
}

void GLDisplay::zoom(double factor, int x, int y)
{
  y = winarea.height - y - 1; // invert row number

  auto newsize = winarea.pxsize*factor;
  winarea.origin = {
    winarea.origin.real() + x * (winarea.pxsize - newsize),
    winarea.origin.imag() + y * (winarea.pxsize - newsize),
  };
  winarea.pxsize = newsize;

  glutPostRedisplay();
  postRedraw();
}

void GLDisplay::toggleFullScreen()
{
  if(fullscreen.isFull)
  {
    glutReshapeWindow(fullscreen.oldwidth, fullscreen.oldheight);
    fullscreen.isFull = false;
  }
  else
  {
    fullscreen.oldwidth = glutGet(GLUT_WINDOW_WIDTH);
    fullscreen.oldheight = glutGet(GLUT_WINDOW_HEIGHT);
    glutFullScreen();
    fullscreen.isFull = true;
  }
}

void GLDisplay::cycleGenerator()
{
  current_generator = (current_generator + 1) % generators.size();
  std::cout << "Using generator: " << generators[current_generator].name
            << std::endl;

  postRedraw();
}

void GLDisplay::setMaxIter(std::size_t maxiter_)
{
  maxiter = std::max(maxiter_, std::size_t(1));
  postRedraw();
}

void GLDisplay::initState()
{
  static const GLfloat g_vertex_buffer_data[] = {
    0, 0,
    1, 0,
    0, 1,
    1, 1,
  };

  static const GLushort g_element_buffer_data[] = { 0, 1, 2, 3 };

  // init window area
  winarea.width = glutGet(GLUT_WINDOW_WIDTH);
  winarea.height = glutGet(GLUT_WINDOW_HEIGHT);
  winarea.pxsize = 2.0/std::min(winarea.width, winarea.height);
  winarea.origin = {
    -0.5*winarea.pxsize*winarea.width - 0.5,
    -0.5*winarea.pxsize*winarea.height,
  };
  // generator id
  current_generator = 0;
  //number of iterations
  maxiter = 10000;

  // texture parameters
  texarea = winarea;
  needsRedraw = false;

  // buffers
  buffer.vertex = make_buffer(GL_ELEMENT_ARRAY_BUFFER, g_vertex_buffer_data,
                              sizeof(g_vertex_buffer_data));
  buffer.element = make_buffer(GL_ELEMENT_ARRAY_BUFFER, g_element_buffer_data,
                               sizeof(g_element_buffer_data));

  // generate and set texture
  glGenTextures(1, &texture);
  if(texture == 0)
    std::abort();

  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);

  // compile and link shader program
  {
    auto vertex_shader = make_shader(GL_VERTEX_SHADER, vertex_shader_src);
    auto fragment_shader = make_shader(GL_FRAGMENT_SHADER,
                                       fragment_shader_src);

    program = make_program(vertex_shader, fragment_shader);
    if (program == 0)
      std::abort();

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
  }


  // determine shader parameter locations
  uniforms.texture   = glGetUniformLocation(program, "texture");
  uniforms.texorigin = glGetUniformLocation(program, "texorigin");
  uniforms.texsize   = glGetUniformLocation(program, "texsize");
  uniforms.winorigin = glGetUniformLocation(program, "winorigin");
  uniforms.winsize   = glGetUniformLocation(program, "winsize");

  attributes.position = glGetAttribLocation(program, "position");

  // mouse info
  mouse.dragging = false;

  // saved window state
  fullscreen.isFull = false;

  // get initial draw going
  postRedraw();
}

//
// callbacks
//

void GLDisplay::idle()
{
  // check whether redraw is ready
  // idle should only be called when there is a computation
  if(newData.wait_for(std::chrono::duration<int>(0))
       != std::future_status::timeout)
  {
    setTexture(newData.get());

    glutPostRedisplay();
    glutIdleFunc(nullptr);

    if(needsRedraw)
      postRedraw(); // may reinstate idle func
  }
}

void GLDisplay::keyboard(unsigned char key, int x, int y)
{
  switch (key)
  {
  case 'f': case 'F':
    toggleFullScreen();
    break;
  case 'g': case 'G':
    cycleGenerator();
    break;
  case 'q': case 'Q':
    std::exit(0);
  case '+':
    zoom(zoomFactor, x, y);
    break;
  case '-':
    zoom(1/zoomFactor, x, y);
    break;
  }
}

void GLDisplay::keyboard_s(int key, int x, int y)
{
  switch (key)
  {
  case GLUT_KEY_UP:        setMaxIter(maxiter *  2); break;
  case GLUT_KEY_DOWN:      setMaxIter(maxiter /  2); break;
  case GLUT_KEY_PAGE_UP:   setMaxIter(maxiter * 10); break;
  case GLUT_KEY_PAGE_DOWN: setMaxIter(maxiter / 10); break;
  }
}

void GLDisplay::mouse_button(int button, int action, int x, int y)
{
  switch(button) {
  case GLUT_LEFT_BUTTON:
    switch(action) {
    case GLUT_DOWN:
      mouse.dragging = true;
      mouse.x = x;
      mouse.y = y;
      break;
    case GLUT_UP:
      mouse.dragging = false;
      break;
    }
    break;
  case 3: // scroll-up
    switch(action) {
    case GLUT_DOWN:
      zoom(zoomFactor, x, y);
      break;
    }
    break;
  case 4: // scroll-down
    switch(action) {
    case GLUT_DOWN:
      zoom(1/zoomFactor, x, y);
      break;
    }
    break;
  }
}

void GLDisplay::mouse_motion(int x, int y)
{
  if(!mouse.dragging)
    return;
  auto offset_x = x - mouse.x;
  auto offset_y = y - mouse.y;

  mouse.x = x;
  mouse.y = y;

  winarea.origin = {
    winarea.origin.real() - offset_x * winarea.pxsize,
    winarea.origin.imag() + offset_y * winarea.pxsize,
  };

  postRedraw();
  glutPostRedisplay();
}

void GLDisplay::render() const
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glUseProgram(program);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  glUniform1i(uniforms.texture, 0);

  glUniform2f(uniforms.texorigin, texarea.origin.real(),
                                  texarea.origin.imag());
  glUniform2f(uniforms.texsize,   texarea.pxsize * texarea.width,
                                  texarea.pxsize * texarea.height);

  glUniform2f(uniforms.winorigin, winarea.origin.real(),
                                  winarea.origin.imag());
  glUniform2f(uniforms.winsize,   winarea.pxsize * winarea.width,
                                  winarea.pxsize * winarea.height);

  glBindBuffer(GL_ARRAY_BUFFER, buffer.vertex);
  glVertexAttribPointer(attributes.position,  /* attribute */
                        2,                    /* size */
                        GL_FLOAT,             /* type */
                        GL_FALSE,             /* normalized? */
                        sizeof(GLfloat)*2,    /* stride */
                        (void*)0              /* array buffer offset */
                        );
  glEnableVertexAttribArray(attributes.position);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.element);
  glDrawElements(GL_TRIANGLE_STRIP,  /* mode */
                 4,                  /* count */
                 GL_UNSIGNED_SHORT,  /* type */
                 (void*)0            /* element array buffer offset */
                 );

  glDisableVertexAttribArray(attributes.position);

  glutSwapBuffers();
}

void GLDisplay::reshape(int width, int height)
{
  winarea.origin = {
    winarea.origin.real() - 0.5 * winarea.pxsize * (width - int(winarea.width)),
    winarea.origin.imag() - 0.5 * winarea.pxsize * (height - int(winarea.height)),
  };
  winarea.width = width;
  winarea.height = height;

  glViewport(0,0,width,height);

  postRedraw();
}

void GLDisplay::run(int argc, char **argv)
{
  instance_ = this;

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
  // glutInitWindowSize(400, 300);
  glutCreateWindow("Mandelbrot");

  glutDisplayFunc (CallBack::render);
  glutKeyboardFunc(CallBack::keyboard);
  glutSpecialFunc (CallBack::keyboard_s);
  glutMouseFunc   (CallBack::mouse_button);
  glutMotionFunc  (CallBack::mouse_motion);
  glutReshapeFunc (CallBack::reshape);

  glewInit();

  if(!GLEW_VERSION_2_0)
  {
    std::cerr << "OpenGL 2.0 not available" << std::endl;
    std::abort();
  }

  initState();

  glutMainLoop();
}

