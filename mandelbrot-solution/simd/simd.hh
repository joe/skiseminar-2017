#ifndef DUNE_COMMON_SIMD_SIMD_HH
#define DUNE_COMMON_SIMD_SIMD_HH

/** @file
 *  @brief Include file for users of the SIMD abstraction layer
 *
 * Include this file if you want to be able to handle SIMD types -- do not
 * include the internal headers directly.
 */

#include "interface.hh"
#include "standard.hh"

#endif // DUNE_COMMON_SIMD_SIMD_HH
