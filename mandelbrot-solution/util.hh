#ifndef SKISEMINAR_UTIL_HH
#define SKISEMINAR_UTIL_HH

#include <algorithm>
#include <complex>
#include <cstddef>
#include <cstdlib>
#include <locale>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include "simd/simd.hh"

using namespace Dune::Simd;

template<class VT>
VT sequence()
{
  VT result;
  for(std::size_t l = 0; l < lanes(result); ++l)
    lane(l, result) = l;
  return result;
}

// get the square of the absolute value
template<class T>
auto abs2(const T &v)
{
  return v*v;
}

// get the square of the absolute value
template<class T>
auto abs2(const std::complex<T> &v)
{
  return abs2(v.real()) + abs2(v.imag());
}

inline bool spaceonly(std::istream &s)
{
  char dummy;
  s >> dummy;
  // now extraction should have failed, and eof should be set
  return s.fail() && s.eof();
}

template<class T>
T parseenv(const char *name, T def)
{
  const char *text = getenv(name);

  if(!text)
    return def;

  { // check whether text is whitespace only
    std::istringstream s(text);
    s.imbue(std::locale::classic());
    if(spaceonly(s))
      return def;
  }

  T val;
  std::istringstream s(text);
  // make sure we are in locale "C"
  s.imbue(std::locale::classic());
  s >> val;
  if(!s)
    throw std::invalid_argument("Can't parse " + std::string(name) + "='" +
                                text + "'");
  if(!spaceonly(s))
    throw std::invalid_argument("Junk at end of " +std::string(name) + "='" +
                                text + "'");
  return val;
}

template<class T>
struct Conf
{
  std::complex<T> center = { -0.5, 0.0 };
  T px_per_unit;
  std::size_t width = 1000;
  std::size_t height = 1000;
  std::size_t maxiter = 10000;
  bool gl = false;

  Conf(int argc, char **argv)
  {
    center  = { parseenv("CENTER_REAL", center.real()),
                parseenv("CENTER_IMAG", center.imag()) };
    width   = parseenv("WIDTH",   width );
    height  = parseenv("HEIGHT",  height);
    maxiter = parseenv("MAXITER", maxiter);
    px_per_unit = parseenv("PX_PER_UNIT",
                           T(std::max(height, width) / 2.0));
    gl      = parseenv("DO_GL", gl);
  }

  void report(std::ostream &str) const
  {
    str << "CENTER_REAL=" << center.real() << "\n"
        << "CENTER_IMAG=" << center.imag() << "\n"
        << "PX_PER_UNIT=" << px_per_unit << "\n"
        << "WIDTH=" << width << "\n"
        << "HEIGHT=" << height << "\n"
        << "MAXITER=" << maxiter << std::endl;
  }
};

#endif // SKISEMINAR_UTIL_HH
