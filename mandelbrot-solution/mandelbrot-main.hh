#ifndef SKISEMINAR_MANDELBROT_MAIN_HH
#define SKISEMINAR_MANDELBROT_MAIN_HH

#include <iostream>

#include "mandelbrot.hh"
#include "simd/simd.hh"
#include "util.hh"

using namespace Dune::Simd;

template<class V, class ColorMap>
auto mandelbrot(const Conf<Scalar<V> > conf, const ColorMap &colorMap)
{
  auto pxsize = 1.0/conf.px_per_unit;
  std::complex<Scalar<V> > origin = {
    conf.center.real() - 0.5 * pxsize * conf.width,
    conf.center.imag() - 0.5 * pxsize * conf.height,
  };
  return mandelbrot<V>(origin, pxsize,
                       conf.width, conf.height, conf.maxiter,
                       colorMap);
}

template<class V>
int mandelbrotMain(int argc, char **argv)
{
  std::cerr << "Lanes: " << lanes<V>() << std::endl;
  Conf<Scalar<V> > conf{argc, argv};
  conf.report(std::cerr);

  auto img = mandelbrot<V>(conf, SmoothColorMap{96});
  img.write(std::cout);
  return 0;
}

#endif // SKISEMINAR_MANDELBROT_MAIN_HH
