#include "mandelbrot-main.hh"
#include "simd/vc.hh"

int main(int argc, char **argv)
{
  return mandelbrotMain<Vc::Vector<double> >(argc, argv);
}
