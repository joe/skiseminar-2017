all: mandelbrot.png scissors.jpg

mandelbrot.png: mandelbrot-solution/mandelbrot_vc
	WIDTH=1440 HEIGHT=1080 PX_PER_UNIT=360 CENTER_REAL=-.2 $< \
	| convert - $@

scissors.jpg: simd-illustration/PB020395.JPG
	convert -resize 1440x1080 $< $@

mandelbrot-solution/mandelbrot_vc:
	echo "Looks like you need to 'make -C mandelbrot-solution mandelbrot_vc'"
	exit 2
