// -*- c -*-
#version 110

// coordinates within ll=(0,0)..ur=(1,1)
attribute vec2 position;

// origin and size of area covered by texture (in the complex plane)
uniform vec2 texorigin;
uniform vec2 texsize;

// origin and size of window area (in the complex plane)
uniform vec2 winorigin;
uniform vec2 winsize;

// coordinates within texture space ll=(0,0)..ur=(1,1)
varying vec2 texcoord;

void main()
{
  // position in the complex plane
  vec2 cpos = position*texsize + texorigin;
  // screen coordinates of vertex ul=(-1,-1)..lr=(1,1)
  gl_Position = vec4((cpos-winorigin)/winsize*vec2(2)-vec2(1), 0.0, 1.0);
  texcoord = position;
}
