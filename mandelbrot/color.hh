#ifndef SKISEMINAR_COLOR_HH
#define SKISEMINAR_COLOR_HH

#include <cassert>
#include <cstddef>
#include <cstdint>

#include "pnm.hh"

struct DefaultColorMap
{
private:
  static constexpr std::uint16_t max_ = 255;

public:
  inline auto operator()(std::size_t value, std::size_t max) const
  {
    static constexpr std::size_t mapsize = 6;
    static const PPM::ColorVal colormap[mapsize] = {
      { max_,    0,    0 },
      { max_, max_,    0 },
      {    0, max_,    0 },
      {    0, max_, max_ },
      {    0,    0, max_ },
      { max_,    0, max_ },
    };

    if(value == max)
      return PPM::ColorVal{ 0, 0, 0 };
    else
      return colormap[value % mapsize];
  }

  std::uint16_t maxcolor() const { return max_; }
};

struct SmoothColorMap
{
private:
  static constexpr std::uint16_t max_ = 255;
  std::size_t cycle_;

  static void setGradient(std::uint16_t &from, std::uint16_t &to,
                          double value)
  {
    assert(value >= 0.0);
    if(value > 1.0)
      setGradient(to, from, 2.0 - value);
    else {
      from = max_;
      to = value*max_;
    }
  }

public:
  SmoothColorMap(std::size_t cycle) : cycle_(cycle) {}

  inline auto operator()(std::size_t value, std::size_t max) const
  {
    PPM::ColorVal color{ 0, 0, 0 };
    if(value == max)
      return color; // black for inside

    value %= cycle_;
    auto fval = (6.0 * value) / cycle_;

    if     (fval < 2.0)
      setGradient(color.red,   color.green, fval);
    else if(fval < 4.0)
      setGradient(color.green, color.blue,  fval - 2.0);
    else // fval < 6.0
      setGradient(color.blue,  color.red,   fval - 4.0);

    return color;
  }

  std::uint16_t maxcolor() const { return max_; }
};

#endif // SKISEMINAR_COLOR_HH
