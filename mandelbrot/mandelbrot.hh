#ifndef SKISEMINAR_MANDELBROT_HH
#define SKISEMINAR_MANDELBROT_HH

#include <complex>
#include <cstddef>
#include <cstdint>

#include "color.hh"
#include "pnm.hh"
// #include "simd/simd.hh"
#include "util.hh"

// using namespace Dune::Simd;

// do the mandelbrot iteration for point c
// stop when the escape criterion is fullfilled or maxiter is reached
// return number of iterations until escape or maxiter
template<class T>
auto iterate(std::complex<T> c, std::size_t maxiter)
{
  // always start from 0+0i
  std::complex<T> z(0);

  // record the number of iterations until escape
  std::size_t iter(maxiter);

  for(std::size_t i = 0; i < maxiter; ++i)
  {
    // do the iteration for all lanes (even escaped ones)
    z = z*z + c;

    // check whether escaped
    auto just_escaped = abs(z) > 2;
    if(just_escaped)
    {
      // record iteration count of escape
      iter = i;
      break;
    }
  }

  return iter;
}

// compute an image of a mandelbrot set for the given area
// use colorMap to assign colors to the number of iterations taken
template<class T, class ColorMap>
auto mandelbrot(std::complex<T> origin, T pxsize,
                std::size_t width, std::size_t height,
                std::uint16_t maxiter, const ColorMap &colorMap)
{
  // image to store result in
  PPM result(width, height, colorMap.maxcolor());

  // row position
  auto y = origin.imag();
  for(std::size_t row = 0; row < height; ++row)
  {
    // reset column position(s)
    auto x = origin.real();
    for(std::size_t col = 0; col < width; ++col)
    {
      // run the computation
      auto iter = iterate(std::complex<T>{ x, y }, maxiter);

      // update image
      result(row, col) = colorMap(iter, maxiter);

      // advance column position(s)
      x += pxsize;
    }

    // advance row position
    y += pxsize;
  }

  return result;
}

#endif // SKISEMINAR_MANDELBROT_HH
