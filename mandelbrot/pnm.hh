#ifndef SKISEMINAR_PNM_HH
#define SKISEMINAR_PNM_HH

#include <cstddef>
#include <cstdint>
#include <limits>
#include <ostream>
#include <vector>

static inline void pnmWriteSample(std::ostream &str, std::uint16_t sample,
                                  std::uint16_t maxval)
{
  if(sample > maxval)
    sample = maxval;
  if(maxval >= 256)
  {
    unsigned char hi = (sample / 256) % 256;
    str.put(reinterpret_cast<char&>(hi));
  }
  unsigned char low = sample % 256;
  str.put(reinterpret_cast<char&>(low));
}

struct PNMBase {
protected:
  std::size_t width_;
  std::size_t height_;
  std::uint16_t maxval_;

  // assumes pixels are stored in "normal english reading order": one row at a
  // time, with the topmost row first.  Columns are numbered left-to-right,
  // rows are numbered bottom-to-top
  std::size_t index(std::size_t row, std::size_t col) const
  {
    return (height_-row-1)*width_ + col;
  }

  PNMBase(std::size_t width, std::size_t height, std::uint16_t maxval) :
    width_(width), height_(height), maxval_(maxval)
  { }
public:
  std::uint16_t maxval() const { return maxval_; }
};

class PGM :
  public PNMBase
{
  // array of pixels in "normal english reading order": one row at a time,
  // with the topmost row first
  std::vector<std::uint16_t> data_;

public:
  PGM(std::size_t width, std::size_t height,
      std::uint16_t maxval = std::numeric_limits<std::uint16_t>::max()) :
    PNMBase{ width, height, maxval }, data_(width*height, 0)
  { }

  // Columns are numbered left-to-right, rows are numbered bottom-to-top
  std::uint16_t &operator()(std::size_t row, std::size_t col)
  {
    return data_[index(row, col)];
  }
  const std::uint16_t &operator()(std::size_t row, std::size_t col) const
  {
    return data_[index(row, col)];
  }

  void write(std::ostream &str) const
  {
    str << "P5\n"
        << width_ << " " << height_ << "\n"
        << maxval_ << "\n";
    static_assert(sizeof(data_[0]) == 2, "");
    for(auto v : data_)
      pnmWriteSample(str, v, maxval_);
    str << std::flush;
  }
};


class PPM :
  public PNMBase
{
public:
  struct ColorVal { std::uint16_t red, green, blue; };

private:
  // array of pixels in "normal english reading order": one row at a time,
  // with the topmost row first
  std::vector<ColorVal> data_;

public:
  PPM(std::size_t width, std::size_t height,
      std::uint16_t maxval = std::numeric_limits<std::uint16_t>::max()) :
    PNMBase{ width, height, maxval }, data_(width*height, {0, 0, 0})
  { }

  // Columns are numbered left-to-right, rows are numbered bottom-to-top
  ColorVal &operator()(std::size_t row, std::size_t col)
  {
    return data_[index(row, col)];
  }
  const ColorVal &operator()(std::size_t row, std::size_t col) const
  {
    return data_[index(row, col)];
  }

  void write(std::ostream &str) const
  {
    str << "P6\n"
        << width_ << " " << height_ << "\n"
        << maxval_ << "\n";
    static_assert(sizeof(data_[0].red) == 2, "");
    for(auto v : data_)
    {
      pnmWriteSample(str, v.red,   maxval_);
      pnmWriteSample(str, v.green, maxval_);
      pnmWriteSample(str, v.blue,  maxval_);
    }
    str << std::flush;
  }
};

#endif // SKISEMINAR_PNM_HH
