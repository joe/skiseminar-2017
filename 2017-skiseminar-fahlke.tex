\documentclass{beamer}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{relsize}
\usepackage[absolute]{textpos}
\usepackage{tikz}

\lstset{
  language=C++,
  basicstyle=\ttfamily,
  tabsize=4,
  keywordstyle=\bfseries\color{red!50!blue},
  commentstyle=\itshape\color{blue},
  emph={implementation_defined,ImplementationDefined},
  emphstyle=\itshape\color{gray},
  emph={[2]\#include,\#define,\#ifdef,\#endif},
  emphstyle={[2]\bfseries\color{red!50!blue}},
  extendedchars=true,
  escapeinside={@}{@},
  breaklines=true,
  defaultdialect=[x86masm]Assembler,
}
\def\asm{\lstinline[language=Assembler]}
\def\cxx{\lstinline[language=C++]}

\AtBeginSection[] % Do nothing for \section*
{
  \frame{\sectionpage}
}

\setbeamertemplate{section page}{
  {%
    \centering%
    \begin{beamercolorbox}[sep=12pt,center]{section title}
      \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}%
  }%
}

\def\fullpageimg#1#2{%
  \tikz\node[inner sep=0pt,#1]{%
    \includegraphics[width=\paperwidth,totalheight=\paperheight]{#2}%
  };%
}

\defbeamertemplate{background}{mandelbrot}[1][opacity=0.2]{%
  \fullpageimg{#1}{mandelbrot}%
}

\defbeamertemplate{background}{scissors}[1][opacity=0.2]{%
  \fullpageimg{#1}{scissors}%
}

\defbeamertemplate{background}{dune}[1][opacity=0.5]{%
  \begin{tikzpicture}[#1,transparency group]
    \node[inner sep=0pt,anchor=south west]{%
      \includegraphics%
        [width=\paperwidth,totalheight=\paperheight]{dune-logo/dune-logo-top}%
    };
    \node[inner sep=0pt,anchor=south west]{%
      \includegraphics[width=\paperwidth]{dune-logo/dune-logo}%
    };
  \end{tikzpicture}%
}

\defbeamertemplate{background}{joe}[1][opacity=0.15]{
  \begin{minipage}{\paperwidth}%
    \centering%
    \resizebox*{!}{\paperheight}{%
      \begin{tikzpicture}[#1,transparency group,line width=2mm,line cap=round]
        \useasboundingbox%[draw,help lines]
          (-.8,-.2) rectangle (1,2.2);
        % \draw[help lines] (-1,0) grid (1,2);
        \draw[x=.8cm] (-.75,1) ..controls(0,2.5)and(1.5,2.5).. (.75,0);
        \draw ellipse[at={(0,0.5)},x radius=0.3,y radius=0.5,rotate=-20];
        \fill ellipse[at={(-.1,1.35)},x radius=1.3mm,y radius=1mm,rotate=25]
              ellipse[at={(.3,1.45)},x radius=1.3mm,y radius=1mm,rotate=20];
      \end{tikzpicture}%
    }%
  \end{minipage}%
}

\def\la{\color{red}}
\def\lb{\color{red!50!green}}
\def\lc{\color{green}}
\def\ld{\color{green!50!blue}}

\def\ln#1{\color{blue}#1}

\newcommand{\simd}[4]{\left\{ {\la#1}, {\lb#2}, {\lc#3}, {\ld#4} \right\}}

\begin{document}

\title{SIMD in Dune}
\subtitle{Designing a Vectorization Abstraction}
\author[Jö]{%
  Jorrit Fahlke\thanks{\texttt{jorrit.fahlke@uni-muenster.de}}
}
\institute{University of Münster}
\date{Skiseminar 2017}

\frame[plain]{\titlepage}

\setbeamertemplate{background}[joe]

\begin{frame}
  \frametitle{Me}

  \begin{itemize}
  \item \alt<1>{I'm Jorrit Fahlke}{Call me Jö}
  \item Born in Witzwort
  \item Grown up mostly near Hannover
  \item Studied Physics in Heidelberg
  \item Switched to computater science for the PhD
  \item Now: Group of Christian Engwer @ WWU
  \item Topic: HPC, Finite Elements\uncover<3>{,\\
      but mostly programming Dune}
  \end{itemize}
\end{frame}

\setbeamertemplate{background}[default]

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

%\section{About Dune}

% - C++, templates
% - grids, grid managers

\setbeamertemplate{background}[scissors]
\section{About SIMD}

% - SIMD-Scheren-Bild
{
  \setbeamertemplate{background}[scissors][]
  \frame{}
}

% - SIMD idea
%   - operations apply lane-by-lane
\begin{frame}
  \frametitle{SIMD Vectors}
  \begin{itemize}
  \item Variable types that contain multiple values.
  \item Operations happen element-wise.
  \item Hardware support: operations happen simultaneously.
  \end{itemize}

  \begin{align*}
    \simd0123 + \simd4567 &= \simd468{10} \\
    \simd0123 \cdot \simd4567 &= \simd05{12}{21} \\
    \frac{\simd0123}{\simd4567} &= \simd0{\frac15}{\frac13}{\frac37} \\
    \sin \simd0{\frac\pi2}\pi{\frac32\pi} &= \simd010{-1}
  \end{align*}
\end{frame}

% - (?)masking
\begin{frame}[fragile]
  \frametitle{SIMD Masks}
  \begin{itemize}
  \item Comparing two simd vectors yields masks.
  \item Essentially simd vectors of \cxx{bool} values.
  \end{itemize}
  \begin{align*}
    \simd7324 < \simd6298 &=
      \simd{\text{no}}{\text{no}}{\text{yes}}{\text{yes}}
  \end{align*}
\end{frame}

% - (?)masking
\begin{frame}[fragile]
  \frametitle{\cxx{if(mask)}}
  \begin{itemize}
  \item Can't work: \cxx{if} requires a \cxx{bool} condition.
  \item Use summary functions with \cxx{bool} result.
  \item Take care that a correct thing still happens for {\em all} lanes.
  \end{itemize}
  \begin{lstlisting}[language=C++,gobble=4]
    for(std::size_t i = 0; i < maxiter; ++i)
    {
      // compute simd vector 'defect'
      auto converged = abs(defect) < cutoff;
      if(allTrue(converged))
        return;
    }
    throw NotConverged{};
  \end{lstlisting}
\end{frame}

% - (?)masking
\begin{frame}[fragile]
  \frametitle{\cxx{mask ? vector1 : vector2}}
  \begin{itemize}
  \item Can't work: \cxx{?:} requires a \cxx{bool} condition.
  \item Replace by a function: \cxx{cond(mask, vector1, vector2)}
  \item Does not short-circuit anymore.
  \end{itemize}
  \begin{lstlisting}[language=C++,gobble=4]
    for(std::size_t i = 0; i < maxiter; ++i)
    {
      // compute simd vectors 'defect', 'update'
      auto converged = abs(defect) < cutoff;
      result = cond(converged, result,
                               result + update);
      if(allTrue(converged))
        return result;
    }
    throw NotConverged{};
  \end{lstlisting}
\end{frame}

% - (?)masking
\begin{frame}[fragile]
  \frametitle{Masking}
  \begin{itemize}
  \item Do operations only where a mask is true.
  \item Has hardware support nowadays.
  \item Not yet part of Dune's proposed SIMD abstraction.
  \end{itemize}
  \begin{lstlisting}[language=C++,gobble=4]
    for(std::size_t i = 0; i < maxiter; ++i)
    {
      // compute simd vectors 'defect', 'update'
      auto converged = abs(defect) < cutoff;
      where(!converged) | result += update;
      if(allTrue(converged))
        return result;
    }
    throw NotConverged{};
  \end{lstlisting}
\end{frame}

% - Data layout
\begin{frame}[fragile]
  \frametitle{Alignment}

  \begin{itemize}
  \item Simd vectors must be aligned to their size.
    \begin{itemize}
    \item 16 bytes for SSE
    \item 32 bytes for AVX
    \item 64 bytes for AVX512
    \end{itemize}
  \item C++ guarantees at most 16 byte alignment
    \begin{itemize}
    \item Works anyway for local variables
    \item Need special care for heap storage\\
      (e.g. \cxx{malloc()}, \cxx{std::vector<SimdType>}).
    \item No way to include simd types in exceptions.
    \end{itemize}
  \item Violating alignment results in a runtime error.
  \end{itemize}
\end{frame}

% - OpenMP vs. Vectorization Libraries
\begin{frame}[fragile]
  \frametitle{Vectorization Libraries}
  \begin{itemize}
  \item Provide special C++ types as described previously
  \item Full control over data layout.
  \item Requires more care during programming
  \item Examples: \href{https://github.com/VcDevel/Vc}{\ln{Vc}} (by M. Kretz),
    \href{http://www.agner.org/optimize/#vectorclass}{\ln{vectorclass/VCL}} (by
    Agner Fog)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{OpenMP 4.0}
  \begin{itemize}
  \item Vectorize by annotating with \cxx{#pragma omp ...}
  \item Can also do multithreading and accelerators.
  \item Can't have global arrays of simd vectors.
  \item[$\Rightarrow$] Not an option for Dune, but maybe for you?
  \end{itemize}
\end{frame}
% - vertical vs. horizontal

\setbeamertemplate{background}[dune]
\section{SIMD in Dune}

\begin{frame}
  \frametitle{Requirements}

  \begin{itemize}
  \item Core classes should be vectorized
  \item No code duplication
  \item Support extended scalar types
  \item Vectorization library is optional
  \item Support multiple vectorization libraries
  \item Allow bindings in user modules
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Core Classes should be Vectorized}

  \begin{itemize}
  \item You should be able to write
    \begin{lstlisting}[language=C++,gobble=6]
      FieldMatrix<SimdType, 3, 3> m =
      {
        ...
      };
      m.invert();
    \end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{No Code Duplication}

  \begin{itemize}
  \item We don't want seperate implementations of \cxx{FieldMatrix} for scalar
    and SIMD types.
  \item Scalar types look just like vector types with one lane.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Support Extended Scalar Types}

  \begin{itemize}
  \item Arbitrary precision types, automatic differenciation types,
    \cxx{std::complex}, \ldots
  \item They worked before, they should keep working
  \item Of course, no actual vectorization for them
  \item Still no code duplication
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Vectorization Library is Optional}

  \begin{itemize}
  \item Stay functional without an external vectoriation library.
  \item Of course, you're restricted to scalar types then.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Support multiple Vectorization Libraries}

  \begin{itemize}
  \item Don't require a particular vectorization library.
  \item Allow to exchange vectorized data with external libraries that {\em
      do} assume a certain vectorization library.
  \item But no interoperability {\em between} vectorization libraries.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Allow Bindings in User Modules}

  \begin{itemize}
  \item Bindings to vectorization libraries can be in user modules.
  \item The core modules will still use them.
  \end{itemize}
\end{frame}

% what do we require for a SIMD library in Dune?
% - ideally not restricted to a single vectorization library
% - must work with fundamental types as well as SIMD vectors
% - (must work with std::complex)
% - must work with extended precision and automatic differentiation types (or
%   anything that resembles a fundamental type)

% lesser requirements
% - make it easy to implement the abstraction for a new vectorization library
%   - provide default implementations

%\setbeamertemplate{background}[default]

%\section{Design}

% - Classes of interests in the interface (abstraction developer, library
%   developer, application(?) developer).
% - Who #includes what?

\setbeamertemplate{background}[mandelbrot]
\section{Mandelbrot}


\begin{frame}
  \frametitle{Mandelbrot Definition}

  The set of all values $c\in\mathbb C$ such that the sequence $z_n$ is
  bounded, where
  \begin{align*}
    z_0    &:= 0 \\
    z_{n+1} &:= z_n^2 + c
  \end{align*}

  Escape criterion: It can be shown that if $|z_n|>2$ for any $n$, then $z_n$
  is unbounded.

  (Source: Wikipedia)
\end{frame}

\begin{frame}
  \frametitle{Mandelbrot Visualization}

  \begin{itemize}
  \item Set a maximum iteration count \cxx{maxiter}.
  \item Consider $c\in\mathbb C$.
  \item Determine smallest iteration $n$ such that $z_n$ has escaped.
  \item Color $c$ according to $n$ (outside points).
  \item If $z_n$ did not escape, assume it is in the set, color black.
  \end{itemize}

  Actually, we aren't visualizing the Mandelbrot set, but its complement!
\end{frame}

\begin{frame}[fragile]
  \frametitle{Live Coding Cheat Sheet}
  \begin{columns}
    \begin{column}{.45\textwidth}
      \smaller[2]
\begin{lstlisting}
V v; // V is a Simd Vector
using S = Scalar<V>;
using M = Mask<V>;
using I = Index<V>;

// access lanes
size_t count = lanes<V>();
size_t count = lanes(v);
S s = lane(0, v);
lane(1, v) = s;

// branching
V abs = cond(v < 0, -v, v);
if(allTrue(m)) { ... }
if(anyTrue(m)) { ... }
if(allFalse(m)) { ... }
if(anyFalse(m)) { ... }

// all the usual operators
// and math functions
// work lane-wise
\end{lstlisting}
    \end{column}
    \begin{column}{.55\textwidth}
      \begin{itemize}
        \item \structure{Mandelbrot:}\\
      all points $c\in\mathbb C$ such that
      \begin{align*}
        z_0    &:= 0, &
        z_{n+1} &:= z_n^2 + c
      \end{align*}
      remains bounded.

      \item \structure{Escape criterion:}\\
      definitely unbounded if
      \begin{align*}
        |z_n| > 2
      \end{align*}
      for any $n$.

      \item \structure{Color:}\\
      Number of iterations until escape criterion reached.
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\setbeamertemplate{background}[default]

\appendix

\section{Backup Slides}

\subsection{SIMD}

\begin{frame}[fragile]
  \frametitle{SIMD: Alignment in Exceptions}

  \begin{lstlisting}[language=C++,gobble=4]
    struct NotConverged : std::runtime_error
    {
      Mask where;
      NotConverged(Mask w);
    };

    // ...

    throw NotConverged{!converged}; // @\alert{error}@
  \end{lstlisting}
\end{frame}

\subsection{Challenges}

% - providing default implementations: name lookup, judicious use of ADL,
%   avoiding name squatting
% - Vc: Incompleteness vs. unit testing, differing ideas of masks/what
%   operations /should/ look like
% - The trouble with type promotions
% - Unit testing: avoiding OOM during compilation: explicit specialization

\end{document}

%%% Local Variables:
%%% mode: latex
%%% mode: TeX-PDF
%%% TeX-master: t
%%% End:
